<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Venda;
use App\Models\Vendedor;

class VendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            "id_vendedor" => 'required|integer',
            "valor" => 'required'
        ]);

        $vendedor = Vendedor::find($data['id_vendedor']);
        
        if (!$vendedor) {
            return response([
                "mensagem" => "Vendedor não encontrado"
            ], 404);
        }

        $data['comissao'] = $data['valor'] * 0.065;

        try {
            $venda = Venda::create($data);

            return response([
                "id" => $venda->id,
                "Nome" => $vendedor->nome,
                "Email" => $vendedor->email,
                "Valor" => $venda->valor,
                "Comissao" => $venda->comissao,
                "Data" => $venda->created_at,
            ],201);
        } catch (\Throwable $th) {
            return response([
                "mensagem" => $th
            ], 500);
        }
        
        
    }

    
}
