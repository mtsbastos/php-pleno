<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vendedor;

class VendedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vendedores = Vendedor::select('id','nome','email',(Vendedor::raw('(select sum(comissao) from vendas where vendas.id_vendedor = vendedores.id) as comissao')) )->get();

        return response(["vendedores" => $vendedores]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'nome' => 'required',
            'email' => 'email|required'
        ]);

        $vendedor = Vendedor::create($data);
        
        
        return response([
            'id' => $vendedor->id,
            'nome' => $vendedor->nome,
            'email' => $vendedor->email
        ],201);
    }

    public function listarVendas($idVendedor) 
    {
        if ($idVendedor > 0) {
            $vendas = Vendedor::select('id','nome','email')
                                    ->with(['vendas' => function($query) {
                                        $query->select(['vendas.id_vendedor', 'vendas.valor','vendas.comissao','vendas.created_at as data']);
                                    }])
                                    ->where('id','=',$idVendedor)->get();        
            return response($vendas);
        }

        return response([
            "mensagem" => "Vendedor não encontrado"
        ], 404);
    }

}
