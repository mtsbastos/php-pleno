<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Venda extends Model
{
    use HasFactory;

    protected $fillable = [
        'id_vendedor',
        'valor',
        'comissao'
    ];

    public function vendedor()
    {
        return $this->hasOne(Vendedor::class,'id','id_vendedor');
    }
}
