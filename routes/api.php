<?php

use App\Http\Controllers\VendaController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VendedorController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('vendedor', [VendedorController::class,'index']);
Route::get('vendas_vendedor/{idVendedor}', [VendedorController::class,'listarVendas']);
Route::post('vendedor', [VendedorController::class, 'store']);

Route::post('venda',[VendaController::class,'store']);